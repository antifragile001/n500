add it backtrader using :> 

    symbol = "TICKER"
    data = btfeeds.GenericCSVData(
        dataname='{}.csv'.import(symbol),
        fromdate=datetime.datetime(2020, 1, 1),
        todate=datetime.datetime(2020, 3, 1),
        nullvalue=0.0,
        dtformat=('%Y-%m-%d'),
        tmformat=('%H:%M:%S'),
        date=0,
        time=1,
        open=2,
        high=3,
        low=4,
        close=5,
        volume=6,
    )